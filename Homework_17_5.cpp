

#include <iostream>

class Vector 
{
public:
	Vector() : x(0), y(0), z(0)
	{}
	
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	
	void Show()
	{
		std::cout << "Vector: " << x << " " << y << " " << z << std::endl;
	}
		
	double Length()
	{
		double vector1, vector2;

		vector1 = pow(x, 2) + pow(y, 2) + pow(z, 2);
		vector2 = sqrt(vector1);

		std::cout << "Length of vector: " << vector2 << std::endl;

		return vector2;
	}

private:
	double x;
	double y;
	double z;
};

class Calc
{

public:
	
	double plus(double val11, double val22) 
	{
		val1 = val11;
		val2 = val22;

		std::cout << "Sum: " << val1 + val2 << std::endl;

		return val1 + val2;
	}

	double minus(double val11, double val22)
	{
		val1 = val11;
		val2 = val22;

		std::cout << "Minus: " << val1 - val2 << std::endl;

		return val1 - val2;
	}

	double multiplication(double val11, double val22)
	{
		val1 = val11;
		val2 = val22;

		std::cout << "Multi: " << val1 * val2 << std::endl;

		return val1 * val2;
	}

	double division(double val11, double val22)
	{
		val1 = val11;
		val2 = val22;

		std::cout << "Divis: " << val1 / val2 << std::endl;

		return val1 / val2;
	}

private:

	double val1, val2;
};




int main()
{
 
	Calc temp;
	temp.plus(2.3, 2.5);
	temp.minus(2.3, 7.5);
	temp.multiplication(3.3, 2.0);
	temp.division(5.2, 2.0);
	
	Vector v(4, 4, 4);
	v.Show();
	v.Length();
	
	return 0;
}


